// create another collection inside b-190-course-booking called "fruits"
/*
    insert the following fruit objects along with their properties
        1st object:
            name: apple
            color: red
            stock: 20
            price: 40
            supplier_id: 1
            onSale: true
            origin: [ Philippines and US ]

        2nd object:
            name: banana
            color: yellow
            stock: 15 
            price: 20
            supplier_id: 2
            onSale: true
            origin: [ Philippines and Ecuador ]

        3rd object:
            name: kiwi
            color: green
            stock: 25 
            price: 50
            supplier_id: 1
            onSale: true
            origin: [ US and China ]

        4th object:
            name:mango
            color: green
            stock: 10
            price: 120
            supplier_id: 2
            onSale: false
            origin: [ Philippines and India ]
*/
db.fruits.insertMany([
    {
        name: "apple",
        color: "red",
        stock: 20,
        price: 40,
        supplier_id: 1,
        onSale: true,
        origin:["Philippines", "US"]
    },
    {
        name: "banana",
        color: "yellow",
        stock: 15,
        price: 20,
        supplier_id: 2,
        onSale: true,
        origin:["Philippines", "Ecuador"]  
    },
    {
        name: "kiwi",
        color: "green",
        stock: 25,
        price: 50,
        supplier_id: 1,
        onSale: true,
        origin:["China", "US"]
    },
    {
        name: "mango",
        color: "green",
        stock: 10,
        price: 120,
        supplier_id: 2,
        onSale: false,
        origin:["Philippines", "India"]
    }
]);

// SECTION
// MongoDB Aggregation
/*
    used to generate manipulated data to perform operations to create filtered results that helps in analyzing data
*/

db.fruits.aggregate([
    {$match: {onSale: true}}
]);

db.fruits.aggregate([
    { $group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    { $project: {_id: 0}}
]);

db.fruits.aggregate([
    { $match: {onSale: true}},
    { $group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
    { $sort: {total: -1}}
]);

db.fruits.aggregate([
    { $group: { _id: "$origin", kinds: {$sum: 1}}}
]);

db.fruits.aggregate([
    { $unwind: "$origin"}
]);

db.fruits.aggregate([
    { $unwind: "$origin"},
    { $group: { _id: "$origin", kinds: {$sum: 1}}}
]);

//SECTION
// Schema Design
/*
    - MongoDB documents categorized into normalized or de-normalized/embedded data
    - normalized data referts to data structure where documents are reffered to each other using their ids for related pieces of information
    - De-normalized data/embedded data design refers to a data structure where related pieces of information are added to a document as an embedded object
*/

var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09123456789"
});

/*MINIACTIVITY*/
/*
    collectionName: suppliers
    name:
    contact:
    owner_id: 
*/
ObjectId("62d54874369f1cd57095adab")
db.suppliers.insert({
    name: "John Smith",
    contact: "09123456789",
    owner_id: ObjectId("62d54874369f1cd57095adab")
});

db.suppliers.insert({
    name: "DEF Fruits",
    contact: "09123456789",
    address: [
        {street: "123 San Jose St.", city: "Manila"},
        {street: "367 Gil Puyat", city: "Makati"}
    ]
})

/*MINIACTIVITY*/
/*
create collections...
    suppliers
        2 variables with ObjectId()
            supplier:
            branch:
        document:
        _id: supplier,
        name: "",
        contact: "",
        branches: [branch]

    branch:
    _id: branch field in suppliers
    name: "",
    address: "",
    city: "",
    supplier_id: supplier field in suppliers

*/
var supplier = ObjectId(), branch = ObjectId();
db.suppliers.insert({
    _id: supplier,
    name: "GHI Fruits",
    contact: "09123456789",
    branches: [branch]
});
db.branches.insert({
    _id: ObjectId("62d5520a369f1cd57095adb2"),
    name: "Something Branch",
    address: "insert address here",
    city: "XYZ City",
    supplier_id: ObjectId("62d5520a369f1cd57095adb1")
})